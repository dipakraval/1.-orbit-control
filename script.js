
var scene,camera,hlight,directionalLight,light,light2,light3,light4,car;
var Pz;
function init(){
    scene = new THREE.Scene();
    // const scene = document.querySelector('#c');
    scene.background = new THREE.Color(0xdddddd);

   

    camera = new THREE.PerspectiveCamera(50,window.innerWidth/window.innerHeight,1,5000);
    // camera.rotation.y = 180 * Math.PI / 180;
    camera.position.set( -255, 80, 0 );
    Pz = 0;

    
    var texture = THREE.ImageUtils.loadTexture( "assets/images/ground.jpg" );
    texture.wrapS = THREE.RepeatWrapping; 
    texture.wrapT = THREE.RepeatWrapping;

    var geo = new THREE.PlaneBufferGeometry(2000, 2000, 8, 8);
    var material = new THREE.MeshLambertMaterial({ map : texture});
    var plane = new THREE.Mesh(geo, material);
    plane.rotateX( - Math.PI / 2);
    scene.add(plane);
    // add ground
    // var geo = new THREE.PlaneBufferGeometry(2000, 2000, 8, 8);
    // var mat = new THREE.MeshBasicMaterial({ color: 0x050505, side: THREE.DoubleSide });
    // var plane = new THREE.Mesh(geo, mat);
    // plane.rotateX( - Math.PI / 2);
    // scene.add(plane);

    // Render
    renderer = new THREE.WebGLRenderer({antialias:false});
    renderer.setSize(window.innerWidth,window.innerHeight);
    document.body.appendChild(renderer.domElement);


    // lights
    hlight = new THREE.AmbientLight (0x101010,100);
    scene.add(hlight);

    directionalLight = new THREE.DirectionalLight(0xbbbbbb,10);
    directionalLight.position.set(0,0,0);
    directionalLight.castShadow = true;
    scene.add(directionalLight);

    light = new THREE.PointLight(0xc1c1c1,10);
    light.position.set(0,300,500);
    scene.add(light);

    light2 = new THREE.PointLight(0xc1c1c1,10);
    light2.position.set(500,100,0);
    scene.add(light2);

    light3 = new THREE.PointLight(0xc1c1c1,10);
    light3.position.set(0,100,-500);
    scene.add(light3);

    light4 = new THREE.PointLight(0xc1c1c1,10);
    light4.position.set(-500,300,500);
    scene.add(light4);

    // loader
    let loader = new THREE.GLTFLoader();
    loader.load('assets/modal/mercedes_benz_amg_sl_-_65_1/scene.gltf', function(gltf){
        
        car = gltf.scene.children[0];
        car.scale.set(50,50,50);
        scene.add(gltf.scene);
        animate();
        
    });

    let controls = new THREE.OrbitControls(camera, renderer.domElement);
    

    // maxAzimuthAngle={Math.PI / 4}
    // maxPolarAngle={Math.PI}
    // minAzimuthAngle={-Math.PI / 4}
    // minPolarAngle={0}
    // controls.addEventListener('change', renderer);
    controls.update();
    document.addEventListener("keydown", onDocumentKeyDown, false);
    
    
}
function animate() {
    renderer.render(scene,camera);
    requestAnimationFrame(animate);
}
function onDocumentKeyDown(event) {
    var keyCode = event.which;
    
    if (keyCode == 38) {
        car.position.x += 0.8;
        car.position.z += Pz;

        camera.position.x += 0.8;
        camera.position.z += Pz;
        // up
    } else if (keyCode == 40) {
        car.position.y -= 1;
        // down
    } else if (keyCode == 39) {
        car.rotation.z -= 0.01;
        Pz += 0.01;
        
        // right
    } else if (keyCode == 68) {
        car.position.x += 1;
        
        // space
    } else if (keyCode == 32) {
        car.position.x = 0.0;
        car.position.y = 0.0;
    }
    animate();
};

init();

